﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace DirUse
{
	public partial class Form1 : Form
	{
		private Brush[] brs;
		private List<DirNode> dirs = new List<DirNode>();
		private DirectoryInfo cwd = null;

		public Form1()
		{
			InitializeComponent();
			this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			brs = new Brush[13];
			brs[0] = Brushes.Red;
			brs[1] = Brushes.SaddleBrown;
			brs[2] = Brushes.Orange;
			brs[3] = Brushes.Gold;
			brs[4] = Brushes.Yellow;
			brs[5] = Brushes.GreenYellow;
			brs[6] = Brushes.Green;
			brs[7] = Brushes.Turquoise;
			brs[8] = Brushes.SkyBlue;
			brs[9] = Brushes.Blue;
			brs[10] = Brushes.Violet;
			brs[11] = Brushes.MediumOrchid;
			brs[12] = Brushes.Magenta;

			//brs[0] = (Brushes.Aqua);
			//brs[1] = (Brushes.Red);
			//brs[2] = (Brushes.Orange);
			//brs[3] = (Brushes.DarkKhaki);
			//brs[4] = (Brushes.Yellow);
			//brs[5] = (Brushes.Blue);
			//brs[6] = (Brushes.Gray);
			//brs[7] = (Brushes.Green);
			//brs[8] = (Brushes.HotPink);
			//brs[9] = (Brushes.Maroon);
			//brs[10] = (Brushes.Brown);
			//brs[11] = (Brushes.Indigo);
			//brs[12] = (Brushes.Violet);

			DirNode dn = new DirNode();
			dn.Name = "Parent";
			dirs.Add(dn);
		}

		private void Sort(List<DirNode> arr)
		{
			for (int i = 0; i < arr.Count; i++)
			{
				DirNode v = arr[i];
				int j = i - 1;
				while (j >= 0 && arr[j].Percent < v.Percent)
				{
					arr[j + 1] = arr[j];
					j--;
				}
				arr[j + 1] = v;
			}
		}

		private struct DirNode
		{
			public string Name;
			public long Bytes;
			public int Folders;
			public int Files;
			public double Percent;
		}

		private DirNode FolderSize(string path)
		{
			DirNode dn = new DirNode();
			dn.Bytes = 0;
			dn.Folders = 0;
			dn.Files = 0;
			dn.Percent = 0;
			try
			{
				DirectoryInfo di = new DirectoryInfo(path);
				dn.Name = path;
				foreach (DirectoryInfo curr in di.GetDirectories())
				{
					DirNode myDI = FolderSize(curr.FullName);
					dn.Bytes += myDI.Bytes;
					dn.Folders += myDI.Folders + 1;
					dn.Files += myDI.Files;
				}
				foreach (FileInfo fi in di.GetFiles())
				{
					dn.Bytes += fi.Length;
					dn.Files++;
				}
			}
			catch { }
			return dn;
		}


		private void panel1_Paint(object sender, PaintEventArgs e)
		{
			lstView.SmallImageList = new ImageList();
			lstView.Items.Clear();
			if (cwd == null)
			{
				return;
			}
			try
			{
				panel1.BringToFront();
				Graphics g = panel1.CreateGraphics();//e.Graphics;
				g.Clear(Form1.DefaultBackColor);
				int height = Math.Min(panel1.ClientRectangle.Height, panel1.ClientRectangle.Width);
				int width = height;

				//float[] percents = {0.1f, 0.3f, 0.15f, 0.05f, 0.075f, 0.005f, 0.07f, 0.03f, 0.2f };
				//double[] percents = { 0.08, 0.1, 0.1, 0.1, 0.1, 0.05,0.05, 0.1, 0.1, 0.12, 0.05,0.03,0.02};


				Sort(dirs);
				double currAngle = 270f;

				g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
				g.FillEllipse(Brushes.Black, new Rectangle(0, 0, width, height));
				//g.DrawEllipse(Pens.Black,new Rectangle(8,8,width+4,height+4));

				double whatsLeft = 1;

				for (int i = 0; i < dirs.Count; i++)
				{
					double thisAngle = 0;
					double p = 0;
					double rAngle = 0;
					double c = 0;
					double centerX = 0;
					double centerY = 0;
					Bitmap b;
					Graphics gr;

					whatsLeft = Math.Round(whatsLeft, 3);
					string thisName = "";
					if (Math.Round(whatsLeft, 3) > 0.09 || ((i > 0) && (dirs[i - 1].Percent > 0.09)))
					{
						thisAngle = dirs[i].Percent * 360f;
						p = dirs[i].Percent;
						thisName = (p * 100).ToString("F") + " " + dirs[i].Name.Substring(dirs[i].Name.LastIndexOf("\\") + 1) + " " + GetFileSizeString(dirs[i].Bytes);
						b = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
						gr = Graphics.FromImage(b);
						gr.Clear(Color.Black);
						gr.FillRectangle(brs[i % ((brs.Length == dirs.Count) ? (brs.Length - 1) : brs.Length)], 0, 0, 16, 16);
						gr.Dispose();
						lstView.SmallImageList.Images.Add(i.ToString(), b);

						lstView.Items.Add(new ListViewItem(thisName, i.ToString()));


						g.FillPie(brs[i % ((brs.Length == dirs.Count) ? (brs.Length - 1) : brs.Length)], new Rectangle(2, 2, width - 4, height - 4), (float)currAngle, (float)thisAngle);
						rAngle = (currAngle + (thisAngle / 2.0)) * Math.PI / 180.0;
						c = width * 0.5f;

						centerX = (c + Math.Cos(rAngle) * c * (1 - p) * .75 - g.MeasureString((p * 100).ToString("F") + "%", new Font("Arial", 10)).Width / 2);
						centerY = (c + Math.Sin(rAngle) * c * (1 - p) * .75 - g.MeasureString((p * 100).ToString("F") + "%", new Font("Arial", 10)).Height / 2);
						g.DrawString((p * 100).ToString("F") + "%", new Font("Arial", 10), Brushes.Black, new PointF((float)centerX, (float)centerY));
						g.Flush();
						currAngle += thisAngle;
						whatsLeft -= p;

					}
					else
					{ //summarize the rest for the pie chart, but add them to the list view
						b = new Bitmap(16, 16, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
						gr = Graphics.FromImage(b);
						gr.Clear(Color.Black);
						//gr.FillRectangle(brs[i % ((brs.Length == dirs.Count) ? (brs.Length - 1) : brs.Length)], 0, 0, 16, 16);
						gr.FillRectangle(Brushes.Gray, 0, 0, 16, 16);
						gr.Dispose();
						lstView.SmallImageList.Images.Add(i.ToString(), b);

						thisAngle = whatsLeft * 360f;
						p = whatsLeft;
						long bytes = 0;
						for (int d = i; d < dirs.Count; d++)
						{
							bytes += dirs[d].Bytes;
							lstView.Items.Add(new ListViewItem((dirs[d].Percent * 100).ToString("F") + " " + dirs[d].Name.Substring(dirs[d].Name.LastIndexOf("\\") + 1) + " " + GetFileSizeString(dirs[d].Bytes), i));
						}
						//thisName = (p * 100).ToString("F") + dirs[dirs.Count-1].name + " " + getFileSizeString(bytes);
						//i = dirs.Count - 1;
						//g.FillPie(brs[i % ((brs.Length == dirs.Count) ? (brs.Length - 1) : brs.Length)], new Rectangle(2, 2, width - 4, height - 4), (float)currAngle, (float)thisAngle);
						g.FillPie(Brushes.Gray, new Rectangle(2, 2, width - 4, height - 4), (float)currAngle, (float)thisAngle);
						rAngle = (currAngle + (thisAngle / 2.0)) * Math.PI / 180.0;
						c = width * 0.5f;

						centerX = (c + Math.Cos(rAngle) * c * (1 - p) * .75 - g.MeasureString((p * 100).ToString("F") + "%", new Font("Arial", 10)).Width / 2);
						centerY = (c + Math.Sin(rAngle) * c * (1 - p) * .75 - g.MeasureString((p * 100).ToString("F") + "%", new Font("Arial", 10)).Height / 2);
						g.DrawString((p * 100).ToString("F") + "%", new Font("Arial", 10), Brushes.Black, new PointF((float)centerX, (float)centerY));
						g.Flush();
						break;
					}
				}
				lstView.Items.Add("Parent");
				//dirNode parentNode = new dirNode();
				//parentNode.name = "Parent";
				//dirs.Add(parentNode);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void PopulateDirs(DirectoryInfo initDir)
		{
			foreach (DirectoryInfo thisDir in initDir.GetDirectories())
			{
				DirNode dn = FolderSize(thisDir.FullName);
				dirs.Add(dn);
			}

			DirNode root = new DirNode();
			root.Bytes = 0;
			root.Folders = 0;
			root.Files = 0;
			root.Name = "\\& Root";
			foreach (FileInfo fi in initDir.GetFiles())
			{
				root.Files++;
				root.Bytes += fi.Length;
			}
			if (root.Files > 0)
			{
				dirs.Add(root);
			}
			int numItems = dirs.Count;
			//percents = new double[numItems];
			long sizeSum = 0;
			int folders = 0;
			int files = 0;
			for (int i = 0; i < dirs.Count; i++)
			{
				sizeSum += dirs[i].Bytes;
				folders += dirs[i].Folders + 1;
				files += dirs[i].Files;
			}

			if (sizeSum > 0)
			{
				for (int i = 0; i < dirs.Count; i++)
				{
					DirNode dn = dirs[i];
					dn.Percent = (double)dirs[i].Bytes / sizeSum;
					dirs[i] = dn;
				}
			}
			string text = files.ToString("###,###,###,###") + Environment.NewLine +
							folders.ToString("###,###,###,###") + Environment.NewLine +
							GetFileSizeString(sizeSum);

			//because this is called within a thread, we have to make threadsafe calls.
			lblSummaryDetails.Invoke(new SetTextCallback(SetText), text);
			lblThreadStatus.Invoke(new SetTextCallback(UpdateStat), "Generating Pie..");
			panel1.Invoke(new RefreshCallback(Refresh));
			lblThreadStatus.Invoke(new SetTextCallback(UpdateStat), "");
		}

		delegate void RefreshCallback();
		private void Refresh()
		{
			panel1.Refresh();
		}

		delegate void SetTextCallback(string text);
		private void UpdateStat(string text)
		{
			lblThreadStatus.Text = text;
		}
		private void SetText(string text)
		{
			lblSummaryDetails.Text = text;
		}

		private void cmdStart_Click(object sender, EventArgs e)
		{
			DialogResult dlg = fldrBrowse.ShowDialog();
			if (dlg == DialogResult.OK)
			{
				StartProc(new DirectoryInfo(fldrBrowse.SelectedPath));
			}
		}

		private void StartProc(DirectoryInfo di)
		{
			try
			{
				//get the items at the root directory
				if (di == null)
				{
					return;
				}
				cwd = di;
				dirs = new List<DirNode>();
				this.Text = "H-DirUse : " + cwd.FullName;
				//dirNode dn = new dirNode();
				//dn.name = "Parent";
				//dirs.Add(dn);

				lblThreadStatus.Text = "Please wait...";

				ThreadStart st = delegate { PopulateDirs(di); };
				Thread t = new Thread(st);
				t.Start();
			}
			catch { }
		}

		private string GetFileSizeString(long bytes)
		{
			return
				(bytes > 1024 ?
				((bytes > 1024 * 1024) ?
				((bytes > 1024 * 1024 * 1024) ?
				(((double)bytes / (1024 * 1024 * 1024)).ToString("N") + "GB")
				: ((double)bytes / (1024 * 1024)).ToString("N") + "MB")
				: (((double)bytes / 1024).ToString("N") + "KB"))
				: (bytes.ToString("N") + "B"));
		}

		private void lstView_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			ListViewHitTestInfo i = lstView.HitTest(x, y);
			if (i.Item != null)
			{
				if (i.Item.Text.Equals("Parent"))
				{
					if (cwd != null)
					{
						StartProc(cwd.Parent);
					}
				}
				else if (dirs[i.Item.Index].Name.IndexOf("&") == -1)
				{
					if (System.IO.Directory.Exists(dirs[i.Item.Index].Name))
					{
						StartProc(new DirectoryInfo(dirs[i.Item.Index].Name));
					}
				}
			}
		}

		int x = 0;
		int y = 0;
		private void lstView_MouseUp(object sender, MouseEventArgs e)
		{
			x = e.X;
			y = e.Y;
		}
	}
}
