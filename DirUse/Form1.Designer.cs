﻿namespace DirUse {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.panel1 = new System.Windows.Forms.Panel();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.lstView = new System.Windows.Forms.ListView();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.cmdStart = new System.Windows.Forms.Button();
			this.lblSummary = new System.Windows.Forms.Label();
			this.lblSummaryDetails = new System.Windows.Forms.Label();
			this.lblThreadStatus = new System.Windows.Forms.Label();
			this.fldrBrowse = new System.Windows.Forms.FolderBrowserDialog();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.panel1.BackColor = System.Drawing.Color.Transparent;
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.MinimumSize = new System.Drawing.Size(100, 100);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(365, 364);
			this.panel1.TabIndex = 0;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.panel1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
			this.splitContainer1.Size = new System.Drawing.Size(571, 364);
			this.splitContainer1.SplitterDistance = 365;
			this.splitContainer1.TabIndex = 2;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.lstView, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.12088F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.87912F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(202, 364);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// lstView
			// 
			this.lstView.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
			this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstView.Location = new System.Drawing.Point(3, 3);
			this.lstView.Name = "lstView";
			this.lstView.ShowGroups = false;
			this.lstView.Size = new System.Drawing.Size(196, 282);
			this.lstView.TabIndex = 1;
			this.lstView.UseCompatibleStateImageBehavior = false;
			this.lstView.View = System.Windows.Forms.View.List;
			this.lstView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstView_MouseDoubleClick);
			this.lstView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.lstView_MouseUp);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.64315F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.35685F));
			this.tableLayoutPanel2.Controls.Add(this.cmdStart, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.lblSummary, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.lblSummaryDetails, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.lblThreadStatus, 0, 1);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 291);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 58.57143F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 41.42857F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(196, 70);
			this.tableLayoutPanel2.TabIndex = 2;
			// 
			// cmdStart
			// 
			this.cmdStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cmdStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.cmdStart.Location = new System.Drawing.Point(118, 44);
			this.cmdStart.Name = "cmdStart";
			this.cmdStart.Size = new System.Drawing.Size(75, 23);
			this.cmdStart.TabIndex = 0;
			this.cmdStart.Text = "Start";
			this.cmdStart.UseVisualStyleBackColor = true;
			this.cmdStart.Click += new System.EventHandler(this.cmdStart_Click);
			// 
			// lblSummary
			// 
			this.lblSummary.AutoSize = true;
			this.lblSummary.Location = new System.Drawing.Point(3, 0);
			this.lblSummary.Name = "lblSummary";
			this.lblSummary.Size = new System.Drawing.Size(60, 39);
			this.lblSummary.TabIndex = 1;
			this.lblSummary.Text = "Files:\r\nDirectories:\r\nBytes:";
			// 
			// lblSummaryDetails
			// 
			this.lblSummaryDetails.AutoSize = true;
			this.lblSummaryDetails.Location = new System.Drawing.Point(92, 0);
			this.lblSummaryDetails.Name = "lblSummaryDetails";
			this.lblSummaryDetails.Size = new System.Drawing.Size(89, 13);
			this.lblSummaryDetails.TabIndex = 2;
			this.lblSummaryDetails.Text = "Nothing Selected";
			// 
			// lblThreadStatus
			// 
			this.lblThreadStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblThreadStatus.AutoSize = true;
			this.lblThreadStatus.Location = new System.Drawing.Point(3, 57);
			this.lblThreadStatus.Name = "lblThreadStatus";
			this.lblThreadStatus.Size = new System.Drawing.Size(0, 13);
			this.lblThreadStatus.TabIndex = 3;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(571, 364);
			this.Controls.Add(this.splitContainer1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "H-DirUse";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.ListView lstView;
		private System.Windows.Forms.FolderBrowserDialog fldrBrowse;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Button cmdStart;
		private System.Windows.Forms.Label lblSummary;
		private System.Windows.Forms.Label lblSummaryDetails;
		private System.Windows.Forms.Label lblThreadStatus;

	}
}

