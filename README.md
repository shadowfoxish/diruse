![](Screenshot.jpg)

# H-DirUse

H-DirUse - View folders on your hard drive as a pie chart, showing you what's taking up space!

In the list view on the right, double click a folder to dig in. Double click "Parent" at the end of the listing to go a level up